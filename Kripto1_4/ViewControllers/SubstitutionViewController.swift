//
//  ViewController.swift
//  Kripto1.3
//
//  Created by Odin on 11/4/14.
//  Copyright (c) 2014 Mordor. All rights reserved.
//

import UIKit

class SubstitutionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
  
  let cellIdentifier = "FrequencyCell"

  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var collectionView: UICollectionView!
  
  var numberFrequencyList = [NumberFrequency]()
  var characterCodes = [String: String]()
 
  override func viewDidLoad() {
    super.viewDidLoad()
  
    self.characterCodes = self.solution()
    self.numberFrequencyList = self.numbersFrequencyFromText(self.textView.text)
  }
  
  override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
    let layout = self.collectionView.collectionViewLayout as UICollectionViewFlowLayout
    if UIInterfaceOrientationIsPortrait(toInterfaceOrientation) {
      layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
    } else {
      layout.scrollDirection = UICollectionViewScrollDirection.Vertical
    }
  }
  
  // MARK: Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let destination = segue.destinationViewController as TextViewController;
    destination.text = self.decodeText(self.textView.text)
  }
  
  // MARK: Private
  
  private func numbersFrequencyFromText(text: String) -> [NumberFrequency] {
    let decimalDigitCharacterSet = NSCharacterSet.decimalDigitCharacterSet()
    let scanner = NSScanner(string: text)
    
    var numbers = [Int: Int]()
    var scannedString: NSString?
    
    var number: Int
    do {
      number = -1
      scanner.scanUpToCharactersFromSet(decimalDigitCharacterSet, intoString: &scannedString)
      scanner.scanInteger(&number)
      if number != -1 {
        if let numberOccurrence = numbers[number] {
          numbers[number] = numberOccurrence + 1
        } else {
          numbers[number] = 1
        }
      }
    } while (number != -1)
    
    var allKeys = Array(numbers.keys)
    allKeys.sort {
      var n1 = numbers[$0]
      var n2 = numbers[$1]
      return n1 > n2
    }
    
    var numbersList = [NumberFrequency]()
    for key in allKeys {
      numbersList.append(NumberFrequency(number: key, frequency: numbers[key]!))
    }
    
    return numbersList
  }
  
  private func solution() -> [String: String] {
    return ["48": "з", "23": "а", "18": "м", "40": "г", "94": "н", "35": "о", "62": "в", "53": "е", "25": "и", "15": "д", "91": "т", "52": "к", "49": "у", "24": "л", "84": "с", "89": "ь", "64": "п", "55": "р", "81": "м", "12": "ш", "60": "ю", "88": "ж", "76": "й", "41": "ч", "27": "х", "31": "б", "38": "я", "29": "ы", "47": "э"]
  }
  
  private func decodeText(text: String) -> String {
    var decodedString = text
    for (key, value) in self.characterCodes {
      decodedString = decodedString.stringByReplacingOccurrencesOfString(key, withString: value, options: nil, range: nil)
    }
    return decodedString
  }
  
  // MARK: UICollectionViewDataSource
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.numberFrequencyList.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    var cell = collectionView.dequeueReusableCellWithReuseIdentifier(self.cellIdentifier, forIndexPath: indexPath) as FrequencyCell
    let numberFrequency = self.numberFrequencyList[indexPath.item]
    cell.numberLabel.text = "\(numberFrequency.number)"
    cell.frequencyLabel.text = "\(numberFrequency.frequency)"
    cell.subTitleLabel.text = "\(indexPath.item)"
    return cell
  }
}

