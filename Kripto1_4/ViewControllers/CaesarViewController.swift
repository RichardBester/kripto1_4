//
//  ViewController.swift
//  Kripto1_4
//
//  Created by Sauron Black on 11/6/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

import UIKit

class CaesarViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
  
  let cellIdentifier = "FrequencyCell"
  let charactersSet = NSCharacterSet.letterCharacterSet().invertedSet
  let theMostUtilisedLetterIndex = 17
  let ukrainianAphabet : [Character] = ["а", "б", "в", "г", "д", "е", "є", "ж", "з", "и", "і", "ї", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ь", "ю", "я"];
  
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var collectionView: UICollectionView!
  
  var characterFrequencyList = [CharacterFrequency]()
  var characterCodes = [String: String]()
  
  var encryptionKey: Int {
    get {
      if let characterFrequency = self.characterFrequencyList.first {
        if let characterIndex = find(self.ukrainianAphabet, characterFrequency.character) {
          return characterIndex - self.theMostUtilisedLetterIndex
        }
      }
      return 0
    }
  }
  
  // MARK: Lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.characterFrequencyList = self.characterFrequenciesFromText(self.textView.text)
  }
  
  // MARK: Overriden
  
  override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
    let layout = self.collectionView.collectionViewLayout as UICollectionViewFlowLayout
    if UIInterfaceOrientationIsPortrait(toInterfaceOrientation) {
      layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
    } else {
      layout.scrollDirection = UICollectionViewScrollDirection.Vertical
    }
  }
  
  // MARK: Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let destination = segue.destinationViewController as TextViewController;
    destination.text = self.decodeText(self.textView.text, withKey: self.encryptionKey)
  }
  
  // MARK: Private
  
  private func characterFrequenciesFromText(text: String) -> [CharacterFrequency] {
    let textComponents = text.componentsSeparatedByCharactersInSet(charactersSet)
    var textFromLettersOnly = join("", textComponents)
  
    var charactersFrequencies = [Character: Int]()
    
    for character in textFromLettersOnly {
      if let characterFrequency = charactersFrequencies[character] {
        charactersFrequencies[character] = characterFrequency + 1
      } else {
        charactersFrequencies[character] = 1
      }
    }
    
    var allKeys = Array(charactersFrequencies.keys)
    allKeys.sort {
      var n1 = charactersFrequencies[$0]
      var n2 = charactersFrequencies[$1]
      return n1 > n2
    }
    
    var charactersList = [CharacterFrequency]()
    for key in allKeys {
      charactersList.append(CharacterFrequency(character: key, frequency: charactersFrequencies[key]!))
    }
    
    return charactersList
  }
  
  private func decodeText(text: String, withKey key: Int) -> String {
    let lowercaseText = text.lowercaseString
    var decodedText = ""
    for character in text {
      if let index = find(self.ukrainianAphabet, character) {
        let shiftedIndex = (index - key + self.ukrainianAphabet.count) % self.ukrainianAphabet.count
        let shiftedCharacter = self.ukrainianAphabet[shiftedIndex]
        decodedText.append(shiftedCharacter)
      } else {
        decodedText.append(character)
      }
    }
    return decodedText
  }
  
  // MARK: UICollectionViewDataSource
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.characterFrequencyList.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    var cell = collectionView.dequeueReusableCellWithReuseIdentifier(self.cellIdentifier, forIndexPath: indexPath) as FrequencyCell
    cell.characterFrequency = self.characterFrequencyList[indexPath.item]
    cell.subTitleLabel.text = "\(indexPath.item)"
    return cell
  }
}

