//
//  TextViewController.swift
//  Kripto1_4
//
//  Created by Odin on 11/10/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

import UIKit

class TextViewController: UIViewController {
  
  @IBOutlet weak var textView: UITextView!
  
  var text: String? = ""
  
  // MARK: Lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.textView.text = self.text!
  }

}
