//
//  NumberFrequescy.swift
//  Kripto1.3
//
//  Created by Odin on 11/4/14.
//  Copyright (c) 2014 Mordor. All rights reserved.
//

import Foundation

public struct CharacterFrequency {
  var character: Character
  var frequency: Int
}
